<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cliente_Transacoes extends Model
{
    protected $table = 'client_fin';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        "nome",
        "cpf", 
    ];
    
    protected $guarded  = [
        'id', 'cadastrado_em'
    ];
    
    /*
     * 
     */
    static public function consultaSaldo($cpf){
       
        $qrySaldo = DB::select("SELECT
                        SUM(valor) valor, 
                        tipo
                        FROM (
                                SELECT *
                                FROM client_history AS A
                                WHERE A.cpf = '{$cpf}'
                        ) AS tmp GROUP BY tipo");
        
        $saldo = 0;                        
        foreach($qrySaldo as $dados){
           if($dados->tipo == 'C'){
               $saldo += $dados->valor;
           }else if($dados->tipo == 'D'){
               $saldo -= $dados->valor;
           }             
        }                        
        
        return $saldo;
        
    }
    
    /*
     * 
     */
    static public function consultaExtrato($cpf){
       
        $qryExtrato = DB::select("SELECT 
                                    id,
                                    tipo, 
                                    cpf, 
                                    valor,
                                    DATE_FORMAT(cadastrado_em, '%d/%m/%Y - %H:%i:%s') AS data_f
                                FROM client_history AS A
                                WHERE A.cpf = '{$cpf}'");
        
        
        return $qryExtrato;
        
    }
}
