<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'app'], function(){
    Route::get("/", "TransacoesController@index"); 
    Route::get("/saldo/{cpf}", "TransacoesController@saldo");
    Route::get("/extrato/{cpf}", "TransacoesController@Extrato");
    Route::get("/debito/{cpf}", "TransacoesController@debito");
    Route::get("/credito/{cpf}", "TransacoesController@credito");
    Route::get("/transferencia/{cpf}", "TransacoesController@transferencia");
    
    Route::post("/debito", "TransacoesController@lancarDebito");
    Route::post("/credito", "TransacoesController@lancarCredito");
    Route::post("/transferencia", "TransacoesController@lancarTransferencia");
});
