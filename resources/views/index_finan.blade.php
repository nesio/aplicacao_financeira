<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>App Financeiro</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <div class="container">
            <h1>Lista de Cliente</h1>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">CPF</th>
                            <th scope="col" colspan="5">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clientes as $k => $values)
                        <tr>
                            <th scope="row">1</th>
                            <td>{{ $values->nome }}</td>
                            <td>{{ $values->cpf }}</td>
                            <td><a href="{{ url("app/saldo/") }}/{{ $values->cpf }} ">Saldo</a></td>
                            <td><a href="{{ url("app/extrato/") }}/{{ $values->cpf }}">Extrato</a></td>
                            <td><a href="{{ url("app/debito/") }}/{{ $values->cpf }}">Débito</a></td>
                            <td><a href="{{ url("app/credito/") }}/{{ $values->cpf }}">Crédito</a></td>
                            <td><a href="{{ url("app/transferencia/") }}/{{ $values->cpf }}">Transferência</a></td>
                        </tr>  
                        @endforeach
                    </tbody>
                </table> 
                @if(!empty($mensagem))
                    <div class="alert alert-info" role="alert">{{ (!empty($mensagem))?$mensagem:'' }}</div>
                @endif   
            </div>
        </div> 
    </body>
</html>