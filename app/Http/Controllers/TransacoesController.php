<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente_Transacoes;
use App\Transacoes;

class TransacoesController extends Controller {
    /*
     * 
     */

    public function index(Request $request) {

        $clientes = Cliente_Transacoes::all();

        return view("index_finan", ["clientes" => $clientes]);
    }

    /*
     * 
     */

    public function saldo(Request $request) {

        $cpf = (isset($request->cpf)) ? $request->cpf : '';
        $saldo = Cliente_Transacoes::consultaSaldo($cpf);


        return view("saldo", ["saldo" => $saldo]);
    }

    /*
     * 
     */

    public function extrato(Request $request) {

        $cpf = (isset($request->cpf)) ? $request->cpf : '';
        $saldo = Cliente_Transacoes::consultaSaldo($cpf);
        $extrato = Cliente_Transacoes::consultaExtrato($cpf);

        return view("extrato", ["saldo" => $saldo, "extrato" => $extrato]);
    }

    /*
     * 
     */

    public function debito(Request $request) {
        $cpf = (isset($request->cpf)) ? $request->cpf : '';
        $saldo = Cliente_Transacoes::consultaSaldo($cpf);

        return view("debito", ["cpf" => $cpf, "saldo" => $saldo]);
    }

    /*
     * 
     */

    public function credito(Request $request) {
        $cpf = (isset($request->cpf)) ? $request->cpf : '';
        $saldo = Cliente_Transacoes::consultaSaldo($cpf);
        
        return view("credito", ["cpf" => $cpf, "saldo" => $saldo]);
    }

    /*
     * 
     */

    public function transferencia(Request $request) {
        $cpf = (isset($request->cpf)) ? $request->cpf : '';
        $saldo = Cliente_Transacoes::consultaSaldo($cpf);

        $favorecidos = Cliente_Transacoes::where("cpf", "!=", $cpf)->get();

        return view("transferencia", ["cpf" => $cpf, "favorecido" => $favorecidos, "saldo" => $saldo]);
    }

    /**
     * 
     */
    public function lancarDebito(Request $request) {

        $valor = str_replace(",", ".", $request->valor);

        $data = [
            'cpf' => $request->cpf,
            'tipo' => 'D',
            'valor' => (double) $valor
        ];

        $clientes = Cliente_Transacoes::all();
        $saldo = Cliente_Transacoes::consultaSaldo($request->cpf);

        if (($saldo - $request->valor) < 0) {
            return view("index_finan", ["clientes" => $clientes, "mensagem" => "Não foi possível realizar operação, saldo insuficiente!"]);
        }

        if (Transacoes::create($data)) {
            return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Operação realizada com sucesso"]);
        } else {
            return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Falha na Operação"]);
        }
    }

    /**
     * 
     */
    public function lancarCredito(Request $request) {

        $data = [
            'cpf' => $request->cpf,
            'tipo' => 'C',
            'valor' => number_format($request->valor, 2, '.', '')
        ];

        $clientes = Cliente_Transacoes::all();

        if (Transacoes::create($data)) {
            return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Operação realizada com sucesso"]);
        } else {
            return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Falha na Operação"]);
        }
    }

    /**
     * 
     */
    public function lancarTransferencia(Request $request) {

        $clientes = Cliente_Transacoes::all();
        $cpfFavorecido = $request->favorecido;
        $cpfDaConta = $request->cpf;
        $verificaSaldo = Cliente_Transacoes::consultaSaldo($cpfDaConta);

        if (($verificaSaldo - $request->valor) > 0) {
            $data = [
                'cpf' => $cpfDaConta,
                'tipo' => 'D',
                'valor' => number_format($request->valor, 2, '.', '')
            ];
            Transacoes::create($data);

            $data2 = [
                'cpf' => $cpfFavorecido,
                'tipo' => 'C',
                'valor' => number_format($request->valor, 2, '.', '')
            ];

            if (Transacoes::create($data2)) {
                return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Transferência realizada com sucesso"]);
            } else {
                return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Falha na Operação"]);
            }
            
        } else {
            return view("../index_finan", ["clientes" => $clientes, "mensagem" => "Falha na Operação, saldo insucifiente!"]);
        }
    }

}
