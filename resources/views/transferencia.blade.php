<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>App Financeiro</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <div class="container">
            <h3>Transferências | Saldo Atual: R$ {{ number_format($saldo,2,',','.') }}</h3>
            <form action="{{ url('/app/transferencia') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="valor">Valor para transferência</label>
                    <input type="text" class="form-control" id="valor" name="valor">
                    <small class="form-text text-muted">O valor lançado, será debitado dessa conta e creditado na conta do favorecido.</small>
                </div> 

                <div class="form-group">
                    <label for="valor">Favorecido</label>
                    <select class="form-select" name="favorecido">
                        <option selected>Selecione um favorecido</option>
                        @foreach($favorecido as $dados)
                        <option value="{{ $dados->cpf }}">{{ $dados->nome }}</option> 
                        @endforeach
                    </select> 
                </div> 

                <input type="hidden" name="cpf" value="{{ $cpf }}" />
                <button type="submit" class="btn btn-primary m-2">Fazer Lançamento</button>
            </form>
            <a href="/app">Voltar</a>
        </div> 
        <script>
            $(document).ready(function () {
                $('#valor').change(function () {
                    this.value = parseFloat(this.value).toFixed(2);
                });
            });
        </script>
    </body>
</html>