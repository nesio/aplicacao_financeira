<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transacoes extends Model
{
    protected $table = 'client_history';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    protected $fillable = [
        "cpf",
        "tipo",
        "valor"
    ];
    
    protected $guarded  = [
        "id",
        "cadastrado_em"
    ];
     
}
