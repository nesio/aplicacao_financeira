<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>App Financeiro</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <div class="container">
            <h3>Extrato aplicação</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Tipo</th> 
                        <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($extrato as $dados)
                        <tr>
                            <th scope="row">{{ $dados->id }}</th>
                            <td>{{ $dados->data_f }}</td>
                            <td>{{ ($dados->tipo == 'C')?'C':'D'  }}</td>
                            <td>{{ number_format($dados->valor,2,',','.') }}</td>
                        </tr> 
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td>Saldo</td>
                        <td>{{ number_format($saldo,2,',','.') }}</td>
                    </tr>
                </tfoot>
            </table>
            <a href="/app">Voltar</a>
        </div> 
    </body>
</html>